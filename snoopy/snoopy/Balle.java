package snoopy;

public class Balle extends Thread {
	
	PanelBalle panel;
	
    private boolean running = true, paused = false;
    static boolean ball=true;
    
    public Balle(int x, int y)
    {
		panel = new PanelBalle(x, y);
    }
    

	public void run() {

		System.out.println("Run de balle");
		while(running)
		{
			this.panel.repaint();	//On rafraichit la balle				/**<----------------------------------------*/
			try {
				Thread.sleep(15);	//On fait une pause de 15ms pour ne pas saturer le proc
				synchronized(this){
                    while (this.paused) wait();
            }
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				this.running=false;
			}
		}
		
	}
	
	protected void initBalle()
	{
		System.out.println("Lancement balle");
		this.start();
	}

	
	@SuppressWarnings("static-access")
	public void pause_balle() { 
    	this.ball=!this.ball;
    	/**Use synchronized to ensure wait() and notify() methods are properly synchronized*/
            synchronized(this){
            	/**if boolean pause is assigned True, is now assigned false and conversely*/
                    this.paused = !this.paused;
                    if (!this.paused) this.notify();
                    
            }
    }
	
	public void stopDeplacementBalle() {
		this.running=false;
		
	}
	

}


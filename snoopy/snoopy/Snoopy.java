package snoopy;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**classe Snoopy*/

public class Snoopy extends Mobile{
	
	/**Declaration attributs*/
	private int orientation=0;
	//private int nblife;
	private static int nblife;
	public boolean gamePause=false;
	private JLabel pauseZone;
	private boolean isinvicible=false;
	
    /**Constructeur par defaut*/
	public Snoopy() 
	{
		super();
		nom="s";
		nblife=3;
	}
	
	
	/**Constructeur surcharge
	 * @param x1
	 * New value*/
	public Snoopy(int x1, int y1, int dx1, int dy1,int posx1,int posy1) 
	{
		super(x1,y1,dx1,dy1,posx1,posy1);
		nom="s";
	}
	
	protected void displaySnoopy()
	{
		System.out.println("X :" + x + "y : " + y);
	}
	
	public void setOrientation(int b) {
		this.orientation=b;
	}
	
	public boolean getInvcible() {
		return isinvicible;
	}
	
	public int getOrientation() {
		return this.orientation;
	}
	
	public static int getNblife() {
		//return this.nblife;
		return nblife;
	}
	
	public static void setNblife(int a) {
		//this.nblife=a;
		nblife=a;
	}
	
	public void setInvicible(boolean a) {
		isinvicible=a;
	}
	
	protected void resetSnoopy()
	{
		this.dx = 1;
		this.dy = 1;
		this.nom = "s";
		this.x = 0;
		this.y = 0;
		this.orientation=0;
	}
	
	public void noMovesPauseMode(JLabel newZone) {
		
		this.gamePause=!this.gamePause;
		this.pauseZone=newZone;
		if(this.gamePause==true) {
			this.pauseZone.setText("Le jeu est en pause");
			
		}
		else this.pauseZone.setText(" ");
		
	}
	
	
	@SuppressWarnings("static-access")
	public void setBoutonDirection() {
		
		
		if(this.getOrientation()==4)Fenetre_Jeu.quelBoutonEstAppuye=Fenetre_Jeu.gauche;
		if(this.getOrientation()==3)Fenetre_Jeu.quelBoutonEstAppuye=Fenetre_Jeu.bas;
		if(this.getOrientation()==2)Fenetre_Jeu.quelBoutonEstAppuye=Fenetre_Jeu.droite;
		if(this.getOrientation()==1)Fenetre_Jeu.quelBoutonEstAppuye=Fenetre_Jeu.haut;
		
		Fenetre_Jeu.quelBoutonEstAppuye.setForeground(Fenetre_Jeu.quelBoutonEstAppuye.getBackground().green);
		Fenetre_Jeu.quelBoutonEstAppuye.setContentAreaFilled(false);
        Fenetre_Jeu.quelBoutonEstAppuye.setOpaque(true);
                 		
	}
	
	@SuppressWarnings("static-access")
	public void resetColorButton() {
		
		Fenetre_Jeu.quelBoutonEstAppuye.setForeground(Fenetre_Jeu.quelBoutonEstAppuye.getBackground().black);
		Fenetre_Jeu.quelBoutonEstAppuye.setContentAreaFilled(true);
        Fenetre_Jeu.quelBoutonEstAppuye.setOpaque(false);
	}
	
	public static int snoopyEstMort() {
		if(getNblife()==0) {
			JOptionPane.showMessageDialog(null, "Vous n'avez plus de vie disponible, le jeu est termine","GAME OVER", JOptionPane.WARNING_MESSAGE);

		}
		return getNblife();
	}
	

}

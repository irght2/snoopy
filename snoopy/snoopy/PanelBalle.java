package snoopy;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

@SuppressWarnings("serial")
/**Classe CustomCanvas*/
public class PanelBalle extends JPanel{
	
	int posx=15, posy=20, dx=-5, dy=5;
	Color color;
	
	
	public PanelBalle(int x, int y)
	{
		posx = x;
		posy = y;
	}
	
	/**Methode pour peindre le canvas*/
	public void paintComponent(Graphics g)
	{
		this.setLayout(new FlowLayout());
		this.setBounds(10, 10, 1000, 500);
		this.setOpaque(false);		
		
		g.setColor(color);
	
			
		
		if(posx > 0 && posx < this.getWidth() && posy > 0 && posy < this.getHeight())
		{
			if(Balle.ball==true) {
				posx = posx + dx;
				posy = posy + dy;	
			}

			g.fillOval(posx, posy, 30, 30);
		}
		else if(posx <= 0 || posx >= getWidth())
		{
			if(Balle.ball==true) {
				dx = -dx;
				posx = posx + dx;
				posy = posy + dy;
				
			}

			color = randomcolor();
			g.fillOval(posx, posy, 30, 30);
		}
		else if(posy <= 0 || posy >= getHeight())
		{
			
			if(Balle.ball==true) {
				
				dy = -dy;
				posx = posx + dx;
				posy = posy + dy;
				
			}

			color = randomcolor();
			g.fillOval(posx, posy, 30, 30);
		}
		
	
	}
	
	/**Methode pour tirer une couleur de maniere aleatoire*/
	protected Color randomcolor()
	{
		Random rand = new Random();
		
		float hue = rand.nextFloat();
		float saturation = 0.9f;
		float luminance = 1.0f; 
		Color rand_color = Color.getHSBColor(hue, saturation, luminance);
		
		return rand_color;
	}

}


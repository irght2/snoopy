package snoopy;

//import java.awt.event.*;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**We would like to thank http://ee402.eeng.dcu.ie. We did not manage to code break mode without his explanation  */

class Counter extends Thread{
	
      private int count = 0;
      private boolean running = true, paused = false;
      private JLabel textZone;
      public boolean noMoves=false;
      
      public Counter(JLabel myZone, int numeroDemarrage){
              this.textZone = myZone;
              //this.attente = attente;
              this.count = numeroDemarrage;
      }
      
      public void run(){
      	
              while(running){
                      this.textZone.setText("Temps restant : " + count-- +" secondes");
                      try{
                              Thread.sleep(1000);
                              synchronized(this){
                                      while (this.paused) wait();
                              }
                      }
                      catch (InterruptedException e){
                              System.out.println("Oops probl�me d'interruption");
                              this.running = false;
                      }
                      
                      //test si le temps a ete depasse
                      this.outOfTime();
                      //if(count==0) {
                      	//JOptionPane.showMessageDialog(null,"Le temps imparti a ete depasse. Vous perdez une vie. Cliquez sur le bouton recommencer pour continuer","GAME OVER", JOptionPane.WARNING_MESSAGE);
       				   /*if(JOptionPane.showConfirmDialog(null, "Le temps imparti a ete depasse, souhaitez vous recommencer")==0) {
       						System.out.println("Ca marche");
       						this.setCount(60);
       					}
       				   
       				   else {
       					   this.running=false;
       				   }*/
                      	
                      	//this.pause_reprise();
                      	//this.setCount(60);
       				   //this.running=false;
       				   //break;
                      //}
              }
      }
      
      public void stopCount() {
      	
      	this.running = false;
      	
      }
      	
      	
      public void pause_reprise() { 
      	
      	/**Use synchronized to ensure wait() and notify() methods are properly synchronized*/
              synchronized(this){
              	/**if boolean pause is assigned True, is now assigned false and conversely*/
                      this.paused = !this.paused;
                      if (!this.paused) this.notify();
                      
              }//return this.paused
      }
      
    
      public void setCount(int StartingValue) {
      	this.count=StartingValue;
      	this.noMoves=false;
      }
      
      public int score() {
    	  return (count+1)*100;
    	  
      }
      
      public int countValue() {
    	  
    	  return count+1;
    	  
      }
      
      public void outOfTime() {
    	  if(count==0) {
            	JOptionPane.showMessageDialog(null,"Le temps imparti a ete depasse. Vous perdez une vie. Cliquez sur le bouton recommencer pour continuer","GAME OVER", JOptionPane.WARNING_MESSAGE);
            	this.noMoves=true;
            	Snoopy.setNblife(Snoopy.getNblife()-1);
            	Snoopy.snoopyEstMort();
            	this.pause_reprise();
            } 
      }
         
      
}
package snoopy;

/**classe mobile*/

public class Mobile extends Pion {
	
	/**Declaration des attributs*/
	int dx, dy,posx,posy;
	
	/**Constructeur par defaut*/
	public Mobile()
	{
		super();
		dx = 0;
		dy = 0;
		posx=0;
		posy=0;
	}
	
	/**Constructeur surcharge
	 * @param x1, y1, dx1, dy1, posx1, posy1
	 * New values*/
	public Mobile(int x1, int y1, int dx1, int dy1,int posx1, int posy1)
	{
		super(x1,y1,null);
		dx = dx1;
		dy = dy1;
		posx=posx1;
		posy=posy1;
	}
	
	/**Getters
	 * @return dx*/
	public int getdX()
	{
		return dx;
	}
	
	public int getdY()
	{
		return dy;
	}
	
	public int getposx()
	{
		return posx;
	}
	
	public int getposy()
	{
		return posy;
	}
	
	/**Setters
	 * @param dx1
	 * value of deplacement*/
	public void setdX(int dx1)
	{
		dx = dx1;
	}
	
	public void setdY(int dy1)
	{
		dy = dy1;
	}
	
	public void setposx(int dx1)
	{
		posx = dx1;
	}
	
	public void setposy(int dy1)
	{
		posy = dy1;
	}

}

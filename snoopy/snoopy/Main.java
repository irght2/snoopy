package snoopy;


/** IMPORTS*/
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**classe Main*/


public class Main implements ActionListener, WindowListener, KeyListener{
	
	/**D�claration des Attributs*/
	static Affichage affichage = new Affichage();
	Pion[][] tableau = new Pion[10][20];
	Fenetre_Jeu fenetre_jeu;
	
	static Snoopy snoopyperso=new Snoopy();		
	ImageIcon bloc_vide = new ImageIcon("images/bloc_vide.png");
	ImageIcon bloc_cassable = new ImageIcon("images/bloc_cassable.png");
	ImageIcon bloc_poussable = new ImageIcon("images/bloc_poussable.png");
	ImageIcon img_oiseau = new ImageIcon("images/oiseau.png");
	ImageIcon img_snoopy = new ImageIcon("images/snoopy.png");
	ImageIcon bloc_piege = new ImageIcon("images/piege.png");
	ImageIcon img_vie = new ImageIcon("images/vie.png");
	ImageIcon img_vortex = new ImageIcon("images/vortex.png");
	ImageIcon img_invincible = new ImageIcon("images/invincible.png");
	int birdcount=0;
	int victorycondition=1;
	
	//test compteur 
	
	int conditionCompteur=0;
	int recommencer=0;
	
	int windowScore=0;
	@SuppressWarnings("rawtypes")
	ArrayList al=new ArrayList();
	int tab[]=new int [20];
	int premierTest=0;
	int nb_niv_save=0;
	
	
	public static void main(String[] args)
	{
		affichage.newMenu();
		affichage.ajoutActionListenerMenu();

	}
		
	/**Definition des ActionListener*/
	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) {
				
		/**Quitte le programme */
		if(e.getSource().equals(affichage.menu.quitter))
		{			
			if(JOptionPane.showConfirmDialog(null, "Vous allez quitter") == 0)
			{
				System.exit(0);
			}
		}
		/**Affiche les regles*/
		else if(e.getSource().equals(affichage.menu.regles))
		{
			System.out.println("regles");
			JOptionPane.showMessageDialog(null, "Snoopy doit parvenir a recupperer les 4 oiseaux avant la fin des 60 secondes.\n"+"Il peut se deplacer avec les fleches du clavier: \n"+"- Appuyer sur A pour casser les blocs cassables\n"	+"- Appuyer sur E pour pousser les blocs poussables\n"+"- Appuyer sur S pour sauvegarder\n"+"- Appuyer sur P pour mettre sur pause\n", "Regles du jeu", JOptionPane.PLAIN_MESSAGE);			
		}
		/**Affiche les scores */
		else if(e.getSource().equals(affichage.menu.scores))
		{
			System.out.println("scores");
			String S=displayScoreInWindow();
			
			if(S==null) JOptionPane.showMessageDialog(null,"Aucun score enregistre","Vos scores", JOptionPane.PLAIN_MESSAGE);
				
			else JOptionPane.showMessageDialog(null,S,"Vos scores", JOptionPane.PLAIN_MESSAGE);
		}
		/**Charge le niveau seauvegarde */
		else if(e.getSource().equals(affichage.menu.charger))
		{
			System.out.println("charger");
			
			int lect4=0;
			File fichier4=new File("niveau/save_nb_niveau.txt");
			Scanner scanner4;
			try {
				scanner4 = new Scanner(fichier4);
				lect4 = scanner4.nextInt();
				scanner4.close();
			} catch (FileNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			
			System.out.println("Voici la valeur de save_niveau sauvegarde : "+lect4);
			
			victorycondition=lect4;
			
			affichage.newFenetre_Jeu(lect4);
			System.out.println("La fenetre cr�ee par charger partie dnne nb_niv_save : "+lect4);
			affichage.ajoutActionListenerFenetre_Jeu();

			try {
				try {
					charger_sauvegarde();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("ca marche");
				remplissage();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			//TESTTTTTTTTTTTTTTTTTTTTTTTTTTTTt
			try {
				affichage.fenetre_jeu.nb_score=read_Data_In_File();
				System.out.println("ConditionCompteur quand on charge une partie : "+conditionCompteur);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			

		}
		/**Acces aux niveaux par mdp */
		else if(e.getSource().equals(affichage.menu.mdp))
		{
			System.out.println("mdp");
			mdp();
		}
		/**Lance le jeu */
		else if(e.getSource().equals(affichage.menu.jouer))
		{
			System.out.println("jouer");
			
			jouer();
			
			
		}
		
		else if(e.getSource().equals(affichage.menu.submit))
		{
			System.out.println("Valide");
			if(affichage.menu.mdp_area.getText().equals("nBpy"))
			{
				affichage.newFenetre_Jeu(1);
				affichage.ajoutActionListenerFenetre_Jeu();
				try {
					charger_tableau();
					remplissage();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				victorycondition=1;
			}
			else if(affichage.menu.mdp_area.getText().equals("aWzp"))
			{
				affichage.newFenetre_Jeu(2);
				affichage.ajoutActionListenerFenetre_Jeu();
				try {
					charger_tableau();
					remplissage();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				victorycondition=2;
			}
			else if(affichage.menu.mdp_area.getText().equals("vZrd"))
			{
				affichage.newFenetre_Jeu(3);
				affichage.ajoutActionListenerFenetre_Jeu();
				try {
					charger_tableau();
					remplissage();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				victorycondition=3;
			}
			else if(affichage.menu.mdp_area.getText().equals("yolo"))
			{
				affichage.newFenetre_Jeu(4);
				affichage.ajoutActionListenerFenetre_Jeu();
				try {
					charger_tableau();
					remplissage();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			victorycondition=4;
			affichage.menu.mdp_frame.dispose();
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.quit))
		{
			System.out.println("Quit");
			
			/**quitte le fil compteur lors de la fermeture de la fenetre jeu (bouton fermer)*/
			affichage.fenetre_jeu.compteur.stopCount();
			birdcount=0;
			victorycondition=1;
			snoopyperso.setNblife(3);
			
			affichage.fenetre_jeu.dispose();
			
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.reset_jeu))
		{
			affichage.fenetre_jeu.compteur.noMoves=false;
			System.out.println("Refresh");
			int cv=affichage.fenetre_jeu.compteur.countValue();
			if(snoopyperso.getNblife()>0 && affichage.fenetre_jeu.compteur.noMoves==false) {
				
			
			try {
				charger_tableau();
				remplissage();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
			
			
			
			if(cv==0) {
				//timeIsOut();
				affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
				affichage.fenetre_jeu.compteur.pause_reprise();
				affichage.fenetre_jeu.compteur.setCount(60);
				
			}
			
			
			
			affichage.fenetre_jeu.compteur.setCount(60);
			snoopyperso.resetColorButton();
			birdcount=0;
			
			//snoopyperso.setOrientation(0);
			
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.gauche))
		{
			System.out.println("gauche");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,4);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
				
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.droite))
		{
			System.out.println("droite");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,2);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.haut))
		{
			System.out.println("haut");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,1);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
		else if(e.getSource().equals(affichage.fenetre_jeu.bas))
		{
			System.out.println("bas");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,3);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
		
		
		/**test bouton casser des blocs*/
		else if(e.getSource().equals(affichage.fenetre_jeu.c)) {
			System.out.println("bouton pour casser");
			casser();
		
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
		
		/**test bouton pousser des blocs*/
		else if(e.getSource().equals(affichage.fenetre_jeu.p)) {
			System.out.println("bouton pour pousser");
			pousser();
		
			affichage.fenetre_jeu.requestFocus();
			affichage.fenetre_jeu.requestFocusInWindow();
		}
	}


	@Override
	public void windowOpened(WindowEvent e) {
		
	}

	/**Definition du comportement de la fenetre lorsqu'elle se ferme*/
	@Override
	public void windowClosing(WindowEvent e) {
		System.out.println("le jeu est termin�");
		System.exit(0);
		
	}


	@Override
	public void windowClosed(WindowEvent e) {		
	}


	@Override
	public void windowIconified(WindowEvent e) {
		
	}


	@Override
	public void windowDeiconified(WindowEvent e) {
		
	}


	@Override
	public void windowActivated(WindowEvent e) {
		
	}


	@Override
	public void windowDeactivated(WindowEvent e) {
		
	}
	
	/**Creation d'une nouvelle fenetre de jeu*/
	protected void jouer()
	{
		affichage.newFenetre_Jeu(1);
		affichage.ajoutActionListenerFenetre_Jeu();
		try {
			charger_tableau();
			remplissage();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**Fontion pour taper un mot de passe pour charger un niveau*/
	protected void mdp()
	{
		affichage.menu.mdp_frame = new JFrame("Mot de passe");
		JPanel content = new JPanel();
		content.setLayout(new FlowLayout());
		affichage.menu.texte = new JLabel("Veuillez entrer un mot de passe :");
		affichage.menu.submit = new JButton("Valider");
		affichage.menu.submit.addActionListener(this);
		affichage.menu.mdp_area = new JTextField(10);
		
		content.add(affichage.menu.texte);
		content.add(affichage.menu.mdp_area);
		content.add(affichage.menu.submit);
		
		affichage.menu.mdp_frame.add(content);
		
		affichage.menu.mdp_frame.setSize(300, 250);
		affichage.menu.mdp_frame.setLocation(300, 300);
		affichage.menu.mdp_frame.setVisible(true);
		affichage.menu.mdp_frame.pack();
	}
	
	/**Fonction chargee du remplissages des JLabel d'apres le tableau de char charge en memoire depuis le fichier*/
	/**@throws FileNotFoundException
	 * Loading file check*/
	public void remplissage() throws FileNotFoundException
	{
		for(int i = 0 ; i<10 ;i++)
		{
			for(int j = 0; j<20; j++)
			{
					
				switch(tableau[i][j].getNom())
				{
					case "t" : affichage.fenetre_jeu.plateau[i][j].setIcon(bloc_piege);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
					
					case "c" : affichage.fenetre_jeu.plateau[i][j].setIcon(bloc_cassable);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
							break;
						
					case "p" : affichage.fenetre_jeu.plateau[i][j].setIcon(bloc_poussable);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
							break;
					
					case "k" : affichage.fenetre_jeu.plateau[i][j].setIcon(bloc_poussable);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
							break;
							
					case "s" : 	affichage.fenetre_jeu.plateau[i][j].setIcon(img_snoopy);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);								
							break;
							
					case "v" : affichage.fenetre_jeu.plateau[i][j].setIcon(bloc_vide);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
						
					case "o" : affichage.fenetre_jeu.plateau[i][j].setIcon(img_oiseau);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
						
					case "l" : affichage.fenetre_jeu.plateau[i][j].setIcon(img_vie);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
						
					case "d" : affichage.fenetre_jeu.plateau[i][j].setIcon(img_vortex);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break; 
					
					case "a" : affichage.fenetre_jeu.plateau[i][j].setIcon(img_vortex);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
					
					case "i" : affichage.fenetre_jeu.plateau[i][j].setIcon(img_invincible);
					affichage.fenetre_jeu.board.add(affichage.fenetre_jeu.plateau[i][j]);
						break;
					
						
					default : System.out.println("Image manquante");
							break;
				}
				refresh();
			}
		}
	}
	
	/**Fonction chargee de charger en memoire le tableau de char*/
	/**@throws FileNotFoundException
	 * Loading file check*/
	public void charger_tableau() throws FileNotFoundException
	{
		int var = 0;
		char carac = 0;
		
		File niveau = new File("niveau/niveau"+ affichage.fenetre_jeu.nb_niveau +".txt");
		
		@SuppressWarnings("resource")
		FileReader fr = new FileReader(niveau);
		
		for(int i = 0 ; i<10 ;i++)
		{
			for(int j = 0; j<20; j++)
			{				
				try {
					var = fr.read();
					if(var != 10 && var !=13)
					{
						carac = (char) var;
						
						switch(carac)
						{
							case 't' : tableau[i][j]=new Pion("t");
									tableau[i][j].setX(j); //On reccupere au passage les coordonnees de chaque pion
									tableau[i][j].setY(i);
									break;
							
							case 'c' : tableau[i][j]=new Pion("c");
									tableau[i][j].setX(j);
									tableau[i][j].setY(i);
									break;
								
							case 'p' : tableau[i][j]=new Pion("p");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
									break;
									
							case 's' : snoopyperso.resetSnoopy();
										tableau[i][j]=snoopyperso;
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
										snoopyperso.displaySnoopy();
									break;
									
							case 'v' : tableau[i][j]=new Pion("v");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'o' : tableau[i][j]=new Pion("o");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'l' : tableau[i][j]=new Pion("l");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'k': tableau[i][j]=new Pion("k");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
							
							case'd' : tableau[i][j]=new Pion("d");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
							
							case'a' : tableau[i][j]=new Pion("a");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
							
							case'i' : tableau[i][j]=new Pion("i");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
								
							
								
							default : System.out.println("bug");
									break;
						}
						
					}
					else
					{
						j--;
					}
				}
				catch(java.io.IOException e)
				{
					System.out.println(e + " Erreur d'ouverture du fichier");
				}
			}
		}
	}
	
	/**Fonction chargee de sauvegarder le tableau de Pion dans un fichier
	 * @throws IOException
	 * FileReader check */
	@SuppressWarnings({ "static-access", "unused" })
	public void sauvegarder_tableau() throws IOException,FileNotFoundException
	{
		
		/**Declaration des fichiers de sauvegardes */
		File fichier1= new File("niveau/save_tableau.txt");
		File fichier2=new File ("niveau/score.txt");
		File fichier3=new File ("niveau/vie.txt");
		File fichier4=new File("niveau/save_nb_niveau.txt");
		File fichier5=new File("niveau/piaf.txt");
		File fichier6=new File("niveau/compteur.txt");
		
		/**objet permettant l'�criture dans les fichiers */
		FileWriter fw1,fw2,fw3,fw4,fw5,fw6;
		int lect4=0;
		
		/**declaration de l'objet mysave ici car intervient dans la boucle ci apr�s*/
		fw1=new FileWriter(fichier1);
		
		
		/**on enregistre les donn�es du jeu (vie, score etc.)*/
		try {
			

			/**Donn�es � enregistrer*/
			int v=snoopyperso.getNblife();
			int currentScore=0;
			int L=affichage.fenetre_jeu.nb_niveau;
			int C=affichage.fenetre_jeu.compteur.countValue();
			
			
			System.out.println("Voici la valeur de L : "+L);
			
			
			fw2=new FileWriter(fichier2);
			fw3=new FileWriter(fichier3);
			fw4=new FileWriter(fichier4);
			fw5=new FileWriter(fichier5);
			fw6=new FileWriter(fichier6);
			
			/**fw2*/
			if(victorycondition>1) {
				currentScore=conditionCompteur;
				System.out.println("Voici le current score ds le ss programe sauegarder tableau : "+currentScore);
				fw2.write(String.valueOf(currentScore));
				//fw2.write("coucou");
				
			} else {
				fw2.write(String.valueOf(0));
			}
			
			fw2.close();
			
			
			/**fw3*/
			fw3.write(String.valueOf(v));
			System.out.println("Voici la vie ds le ss programme sauvegarder tableau : "+v);
			fw3.close();
			
			/**fw4*/
			fw4.write(String.valueOf(L));
			System.out.println("Voici le level ds le ss programe sauegarder tableau : "+L);
			fw4.close();
			
			/**fw5*/
			fw5.write(String.valueOf(birdcount));
			System.out.println("le ss programme sauvegarder renvoie la valeur birdcount : "+birdcount);
			fw5.close();
			
			/**fw6*/
			fw6.write(String.valueOf(C));
			System.out.println("le ss programme sauvegarder renvoie la valeur compteur : "+C);
			fw6.close();
			
			
			
		}catch (FileNotFoundException e) {
		      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
		
		
	/**on enregistre l'etat du plateau de jeu*/
		for(int i = 0 ; i<10 ;i++)
		{
			for(int j = 0; j<20; j++)
			{				
				try {
					
					String dataToWrite=tableau[i][j].getNom();
					
					fw1.write(dataToWrite);
					
					/*System.out.println("case "+":"+tableau[i][j].getNom());
					
					if(tableau[i][j].getNom()=="s") {
						System.out.println("SNOOPYYYYYYYYYYY");
					}*/
					
				}
				catch(java.io.IOException e)
				{
					System.out.println(e + " Erreur d'ouverture du fichier");
					
				}
			}
		}fw1.close();
		
	}

	
	@SuppressWarnings({ "static-access", "unused" })
	public void deplacer(int dx, int dy, int direction) throws FileNotFoundException, IOException  //1 - haut, 2 - droite, 3 - bas, 4 - gauche
	{
		if(snoopyperso.gamePause==false && affichage.fenetre_jeu.compteur.noMoves==false) {
			
			int posxtp=0;
			int posytp=0;
			
			boolean coll=false;
			coll=collision();
			
			if(coll==true) {
				blocpiege();
			}
			
			
			/**on verifie la condition adequate avant chaque deplacement*/
			switch(direction)
			{
			case 1 :
				
				//direction de snoopy
				snoopyperso.setOrientation(direction);
				System.out.println("Voila votre direction: " +snoopyperso.getOrientation());
					
				if(snoopyperso.getY()!=0) {
					
					if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom() == "v") {					
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()-dy);
						
					
					}
					else if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom() == "o") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()-dy);
						
						victoire();
						System.out.println("Le sous programme victoire a fonctionn�");

					}
					else if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom()== "d") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(1);
						snoopyperso.setX(17);						
					}
					else if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom()== "a") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(9);
						snoopyperso.setX(2);						
					}
					
					else if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom() == "t") {
						
						blocpiege();
						
					}
					else if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom() == "l") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()-dy);
						snoopyperso.setNblife(snoopyperso.getNblife()+1);		
						affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
					}
		
				}
				
				else snoopyperso.setY(snoopyperso.getY());
				
					break;
					
			case 2 :
				
				
				snoopyperso.setOrientation(direction);
				System.out.println("Voila votre direction: " +snoopyperso.getOrientation());
				
				if(snoopyperso.getX()!=19) {
					
					if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom() == "v" ) {
						
					tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
					snoopyperso.setX(snoopyperso.getX()+dx);	
					
				}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom() == "o") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setX(snoopyperso.getX()+dx);
						
						victoire();
						System.out.println("Le sous programme victoire a fonctionn�");
						
					}
					
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom() == "t") {
						
						blocpiege();
						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom()== "d") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(1);
						snoopyperso.setX(17);						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom()== "a") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(9);
						snoopyperso.setX(2);						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom() == "l") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setX(snoopyperso.getX()+dx);
						snoopyperso.setNblife(snoopyperso.getNblife()+1);		
						affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
					}
		
			}
						
			
				else snoopyperso.setX(snoopyperso.getX());
					
					break;
					
			case 3 :
				
				
				snoopyperso.setOrientation(direction);
				System.out.println("Voila votre direction: " +snoopyperso.getOrientation());
				
				if(snoopyperso.getY()!=9) {
					
					if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom() == "v") {
					
						
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()+dy);		
						
					}
					else if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom() == "o") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()+dy);
						
						victoire();
						System.out.println("Le sous programme victoire a fonctionn�");
						
					}
					
					else if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom() == "t") {
						
						blocpiege();					
					}
					else if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom()== "d") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(1);
						snoopyperso.setX(17);						
					}
					else if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom()== "a") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(9);
						snoopyperso.setX(2);						
					}
					else if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom() == "l") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(snoopyperso.getY()+dy);
						snoopyperso.setNblife(snoopyperso.getNblife()+1);			
						affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
					}
						
			}
				
				else snoopyperso.setY(snoopyperso.getY());
				
					break;
					
			case 4 : 
				
				
				snoopyperso.setOrientation(direction);
				System.out.println("Voila votre direction: " +snoopyperso.getOrientation());
				
				if(snoopyperso.getX()!=0) {
					
					if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom() == "v") {
						
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setX(snoopyperso.getX()-dx);	
						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom() == "o") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setX(snoopyperso.getX()-dx);
						victoire();
						System.out.println("Le sous programme victoire a fonctionn�");
						
					}
					
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom() == "t") {
						
						blocpiege();
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom()== "d") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(1);
						snoopyperso.setX(17);						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom()== "a") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setY(9);
						snoopyperso.setX(2);						
					}
					else if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom() == "l") {
						tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("v");
						snoopyperso.setX(snoopyperso.getX()-dx);
						snoopyperso.setNblife(snoopyperso.getNblife()+1);
						affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
					}	
			}
				
				else snoopyperso.setX(snoopyperso.getX());
			
					break;
					
			default : System.out.println("Erreur : direction impossible");
					break;
			}
			
			tableau[snoopyperso.getY()][snoopyperso.getX()].setNom("s");
		
			snoopyperso.setBoutonDirection();
			//blocpiege();
			//timeOutOfBounds();
			//timeIsOut();
			
			
		try {
			remplissage();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		refresh();
		
	}
}
	
	public void refresh()
	{
		affichage.fenetre_jeu.east.revalidate();
		affichage.fenetre_jeu.east.repaint();
	}
	
	
	/**Sous programme cassable a appeler dans le action listener correspondant*/
	public void casser() {
		
		if(snoopyperso.getOrientation()==1) {
			
			System.out.println("Je suis colle au mur dans la direction 1.");
			
			if(snoopyperso.getY()-1>=0) {
				
				if(tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom()=="c") {
					tableau[snoopyperso.getY()-1][snoopyperso.getX()].setNom("v");
					
					refresh();
				}
				
			}
			
		}
		
		else if(snoopyperso.getOrientation()==2) {
			
			System.out.println("Je suis colle au mur dans la direction 2.");
			
			if(snoopyperso.getX()+1<=19) {
				
				if(tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom()=="c") {
					tableau[snoopyperso.getY()][snoopyperso.getX()+1].setNom("v");
					
					refresh();
				}
				
			}

		}
		
		else if(snoopyperso.getOrientation()==3) {
			
			System.out.println("Je suis colle au mur dans la direction 3.");
			
			if(snoopyperso.getY()+1<=9) {
				
				if(tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom()=="c") {
					tableau[snoopyperso.getY()+1][snoopyperso.getX()].setNom("v");
					
					refresh();
				}
				
			}

		}
		
		else if(snoopyperso.getOrientation()==4) {
			
			System.out.println("Je suis colle au mur dans la direction 4.");
			
			if(snoopyperso.getX()-1>=0) {
				
				if(tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom()=="c") {
					tableau[snoopyperso.getY()][snoopyperso.getX()-1].setNom("v");
					
					refresh();
				}
				
			}

		}
		
	}
	
	/**Sous programme pousser a appeler dans le action listener correspondant*/
	public void pousser() {
		
		if(snoopyperso.getOrientation()==1) {
			
			System.out.println("Je suis colle au mur dans la direction 1.");
			
			if(snoopyperso.getY()-2>=0) {
				
				if(tableau[snoopyperso.getY()-2][snoopyperso.getX()].getNom()=="v"&&tableau[snoopyperso.getY()-1][snoopyperso.getX()].getNom()=="p") { //on teste si la case au dessus du bloc est vide
					tableau[snoopyperso.getY()-1][snoopyperso.getX()].setNom("v");//si oui le bloc devient "vide"et on le deplace d'une case
					tableau[snoopyperso.getY()-2][snoopyperso.getX()].setNom("k");
					
					refresh();
				}
				
			}
			
			else System.out.println("erreur deppassement de tableau");

		}
		
		else if(snoopyperso.getOrientation()==2) {
			
			System.out.println("Je suis colle au mur dans la direction 2.");
			
			if(snoopyperso.getX()+2<=19) {
				
				if(tableau[snoopyperso.getY()][snoopyperso.getX()+2].getNom()=="v"&&tableau[snoopyperso.getY()][snoopyperso.getX()+1].getNom()=="p") {//on teste si la case a droite du bloc est vide
					tableau[snoopyperso.getY()][snoopyperso.getX()+1].setNom("v");//si oui le bloc devient "vide"et on le deplace d'une case
					tableau[snoopyperso.getY()][snoopyperso.getX()+2].setNom("k");
					
					refresh();
				}
				
			}
			else System.out.println("erreur deppassement de tableau");

		}
		
		else if(snoopyperso.getOrientation()==3) {
			
			System.out.println("Je suis colle au mur dans la direction 3.");
			
			if(snoopyperso.getY()+2<=9) {
				
				if(tableau[snoopyperso.getY()+2][snoopyperso.getX()].getNom()=="v"&&tableau[snoopyperso.getY()+1][snoopyperso.getX()].getNom()=="p") { //on teste si la case en dessus du bloc est vide
					tableau[snoopyperso.getY()+1][snoopyperso.getX()].setNom("v");//si oui le bloc devient "vide"et on le deplace d'une case
					tableau[snoopyperso.getY()+2][snoopyperso.getX()].setNom("k");
					
					refresh();
				}
				
			}
			else System.out.println("erreur deppassement de tableau");

		}
		
		else if(snoopyperso.getOrientation()==4) {
			
			System.out.println("Je suis colle au mur dans la direction 4.");
			
			if(snoopyperso.getX()-2>=0) {
				
				if(tableau[snoopyperso.getY()][snoopyperso.getX()-2].getNom()=="v"&&tableau[snoopyperso.getY()][snoopyperso.getX()-1].getNom()=="p") {//on teste si la case a droite du bloc est vide
					tableau[snoopyperso.getY()][snoopyperso.getX()-1].setNom("v");//si oui le bloc devient "vide"et on le deplace d'une case
					tableau[snoopyperso.getY()][snoopyperso.getX()-2].setNom("k");
					
					refresh();
				}
				
			}
			else System.out.println("erreur deppassement de tableau");
	
		}
		
	}
	

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**Definition des KeyListener*/

	@SuppressWarnings("static-access")
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode()==e.VK_UP)
		{
			System.out.println("fleche haut ou z");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,1);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
		}
		else if(e.getKeyCode()==e.VK_LEFT)
		{
			System.out.println("fleche gauche ou q");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,4);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
		}
		else if(e.getKeyCode()==e.VK_DOWN)
		{
			System.out.println("fleche bas ou s");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,3);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
		}
		else if(e.getKeyCode()==e.VK_RIGHT)
		{
			System.out.println("fleche droite ou d");
			snoopyperso.setOrientation(0);
			snoopyperso.resetColorButton();
			try {
				deplacer(1,1,2);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			refresh();
			snoopyperso.displaySnoopy();
		}
		
		else if(e.getKeyChar() == 'a')
		{
			System.out.println("touche a pour casser");
			casser();
			refresh();
			snoopyperso.displaySnoopy();
		}
		
		else if(e.getKeyChar() == 'e')
		{
			System.out.println("touche e pour pousser");
			pousser();
			refresh();
			snoopyperso.displaySnoopy();
		}
		
		else if(e.getKeyChar() == 'p')
		{
			System.out.println("touche p pour pause");
			snoopyperso.noMovesPauseMode(affichage.fenetre_jeu.textJeuPause);
			affichage.fenetre_jeu.compteur.pause_reprise();
			affichage.fenetre_jeu.balle.pause_balle();
			

		}
		
		else if(e.getKeyChar() == 's')
		{
			System.out.println("touche c pour charger");
			
			try {
				
				sauvegarder_tableau();
				System.out.println("la touche s renvoie la valeur de nb_niv_save : "+nb_niv_save);
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
					
		}
		
	}
	
	
	@SuppressWarnings("static-access")
	public void victoire() throws FileNotFoundException, IOException {
		
	
			birdcount=birdcount+1;
			System.out.println("voici le nombre d'oiseau mang� : "+birdcount);
			System.out.println("count : "+birdcount);
			
			if(birdcount==4) {
				birdcount=0;
				victorycondition=victorycondition+1;

				
				if(victorycondition<5) {
					
					affichage.fenetre_jeu.setNiveau(victorycondition);
					
					System.out.println("voici la valeur de score (conditionCompteur) ds le ss P vctoire : "+conditionCompteur);
					conditionCompteur=affichage.fenetre_jeu.displayScore();
					affichage.fenetre_jeu.compteur.setCount(60);
					affichage.fenetre_jeu.playerScore.setText("Votre score est : "+conditionCompteur);
					
				}
			
				try {
					charger_tableau();
					remplissage();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		
		
		if(victorycondition==5){
			
			victorycondition=1;

			affichage.fenetre_jeu.compteur.stopCount();
			score2();
			
			snoopyperso.setNblife(3);

		    JOptionPane.showMessageDialog(null, "Vous avez gagne !!! \n Felicitations !! ", "Victoire", JOptionPane.WARNING_MESSAGE);
		    affichage.fenetre_jeu.compteur.stopCount();
			affichage.fenetre_jeu.dispose();
			
		}
			
	}
	
	@SuppressWarnings("static-access")
	public void died() {
		/*if(snoopyperso.getNblife()==0)*/
		int w=snoopyperso.snoopyEstMort();
		if(w==0){
			//JOptionPane.showMessageDialog(null, "Vous n'avez plus de vie disponible, le jeu est termine","GAME OVER", JOptionPane.WARNING_MESSAGE);
			affichage.fenetre_jeu.compteur.stopCount();
			victorycondition=1;
			snoopyperso.setNblife(3);
			affichage.fenetre_jeu.dispose();
		}
	}
	
	
	@SuppressWarnings("static-access")
	public void blocpiege() {
		
			snoopyperso.setNblife(snoopyperso.getNblife()-1);
			birdcount=0;
		    
		    
		    JOptionPane.showMessageDialog(null, "Vous avez marche sur un piege. \n Vous perdez une vie. \n Vies restantes : "+Integer.toString(snoopyperso.getNblife()), "Un piege ca fait mal !!", JOptionPane.WARNING_MESSAGE);
		    
			
			try {
				charger_tableau();
				remplissage();
				
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		    
			affichage.fenetre_jeu.compteur.setCount(60);
			/**changer nombre de vie dans la partie nord de la fenetre*/
			affichage.fenetre_jeu.vie.setText("Vie(s) : "+ snoopyperso.getNblife());
			snoopyperso.resetColorButton();
			
			died();
		
	}
	

	@SuppressWarnings("unchecked")
	public void score2() {
	
		windowScore=affichage.fenetre_jeu.displayScore();
		al.add(windowScore);
		
	}
	
	public String displayScoreInWindow() {
		
		return "Scores de vos parties : "+al;
		
	}
	
	
	@SuppressWarnings("unused")
	public boolean collision() {
		boolean test=false;
		
		
		
		int posax1=affichage.fenetre_jeu.balle.panel.posx;		//centtre balle
		int posay1=affichage.fenetre_jeu.balle.panel.posy;
		
		int posbx1=affichage.fenetre_jeu.balle.panel.posx;		//centre balle +30 px vers le haut
		int posby1=affichage.fenetre_jeu.balle.panel.posy-30;
		
		int poscx1=affichage.fenetre_jeu.balle.panel.posx+30;	//centre balle +30 px vers la droite
		int poscy1=affichage.fenetre_jeu.balle.panel.posy;
		
		int posdx1=affichage.fenetre_jeu.balle.panel.posx;		//centre balle +30 px vers le bas
		int posdy1=affichage.fenetre_jeu.balle.panel.posy+30;
		
		int posex1=affichage.fenetre_jeu.balle.panel.posx-30;	//centre balle +30 px vers la gauche
		int posey1=affichage.fenetre_jeu.balle.panel.posy;
		
		
		pixelsnoopy();
		
		if((snoopyperso.posx>posex1 && snoopyperso.posx<posax1)&&(snoopyperso.posy>posby1&&snoopyperso.posy<posey1)) { //test du quart -cos/sin du cercle
			test =true;
		}
		
		if((snoopyperso.posx>posex1 && snoopyperso.posx<posax1)&&(snoopyperso.posy>posey1&&snoopyperso.posy<posdy1)) { //test du quart -cos/-sin du cercle
			test=true;
		}
		
		if((snoopyperso.posx>posax1 && snoopyperso.posx<poscx1)&&(snoopyperso.posy>posby1&&snoopyperso.posy<posay1)) { //test du quart cos/sin du cercle
			test=true;
		}
		
		if((snoopyperso.posx>posax1 && snoopyperso.posx<poscx1)&&(snoopyperso.posy>poscy1&&snoopyperso.posy<posdy1)) { //test du quart cos/-sin du cercle
			test=true;
		}
		return test;
	}
	
	
	
	public void pixelsnoopy() {
		
		
		for(int i=0;i<20;i++) {
			for(int j=0;j<10;j++) {
				if(tableau[j][i]==snoopyperso) {
					snoopyperso.setposx(25+i*50);
					snoopyperso.setposy(25+j*50);
				}
			}
		}
		
	}
	
	public void charger_sauvegarde() throws FileNotFoundException,IOException {
		
		read_Data_In_File();
		int var = 0;
		char carac = 0;
		
		File fichier1= new File("niveau/save_tableau.txt");
		FileReader fr = new FileReader(fichier1);
		
		for(int i = 0 ; i<10 ;i++)
		{
			for(int j = 0; j<20; j++)
			{				
				try {
					var = fr.read();
					if(var != 10 && var !=13)
					{
						carac = (char) var;
						
						switch(carac)
						{
							case 't' : tableau[i][j]=new Pion("t");
									tableau[i][j].setX(j); //On reccupere au passage les coordonnees de chaque pion
									tableau[i][j].setY(i);
									break;
							
							case 'c' : tableau[i][j]=new Pion("c");
									tableau[i][j].setX(j);
									tableau[i][j].setY(i);
									break;
								
							case 'p' : tableau[i][j]=new Pion("p");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
									break;
									
							case 's' : snoopyperso.resetSnoopy();
										tableau[i][j]=snoopyperso;
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
										snoopyperso.displaySnoopy();
									break;
									
							case 'v' : tableau[i][j]=new Pion("v");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'o' : tableau[i][j]=new Pion("o");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'l' : tableau[i][j]=new Pion("l");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
								
							case 'k': tableau[i][j]=new Pion("k");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
								break;
										
										
							case'd' : tableau[i][j]=new Pion("d");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
									break;
				
							case'a' : tableau[i][j]=new Pion("a");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
									break;
				
							case'i' : tableau[i][j]=new Pion("i");
										tableau[i][j].setX(j);
										tableau[i][j].setY(i);
									break;
								
							default : System.out.println("bug");
									break;
						}
						
					}
					else
					{
						j--;
					}
				}
				catch(java.io.IOException e)
				{
					System.out.println(e + " Erreur d'ouverture du fichier");
				}
			}
		}fr.close();
		
		
	}
	
	@SuppressWarnings("unused")
	public int read_Data_In_File() throws FileNotFoundException,IOException {
		
		File fichier2=new File ("niveau/score.txt");
		File fichier3=new File ("niveau/vie.txt");
		File fichier4=new File("niveau/save_nb_niveau.txt");
		File fichier5=new File("niveau/piaf.txt");
		File fichier6=new File("niveau/compteur.txt");
		int lect2=0,lect3=0,lect4=0,lect5=0,lect6=0;

		Scanner scanner2 = new Scanner(fichier2);
		lect2 = scanner2.nextInt();
		System.out.println("Voici la valeur de score sauvegarde : "+lect2);
		scanner2.close();
		
		Scanner scanner3= new Scanner(fichier3);
		lect3 = scanner3.nextInt();
		System.out.println("Voici la valeur de vie sauvegarde : "+lect3);
		scanner3.close();
		
		Scanner scanner5= new Scanner(fichier5);
		lect5 = scanner5.nextInt();
		System.out.println("Voici la valeur de piaf sauvegarde : "+lect5);
		scanner5.close();
		
		Scanner scanner6= new Scanner(fichier6);
		lect6 = scanner6.nextInt();
		System.out.println("Voici la valeur de compteur sauvegarde : "+lect6);
		scanner6.close();
		
	
		affichage.fenetre_jeu.playerScore.setText("Votre score est : "+lect2);
		affichage.fenetre_jeu.vie.setText("Vie(s) : "+ lect3);
		birdcount=lect5;
		//conditionCompteur+=lect2;
		System.out.println("Voici la valeur de bircount"+birdcount); 
		affichage.fenetre_jeu.compteur.setCount(lect6);
		
		return lect2;
		

	}
	
}

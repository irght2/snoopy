package snoopy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**Classe Fenetre_Jeu*/
@SuppressWarnings("serial")
public class Fenetre_Jeu extends JFrame implements WindowListener{
	
	/**Attributs*/
	JButton quit, reset_jeu;
	static JButton haut;
	static JButton bas;
	static JButton droite;
	static JButton gauche;
	JButton c;
	JButton p;
	JLabel temps, vie, leg1, leg2, leg3, leg4, leg5, leg6,textJeuPause,playerScore;
	JPanel north, west, south, line0, line1, line2, line3, panel_bouton, legende, board;
	JLayeredPane east;
	ImageIcon bloc_vide = new ImageIcon("images/bloc_vide.png");
	ImageIcon bloc_cassable = new ImageIcon("images/bloc_cassable.png");
	ImageIcon bloc_poussable = new ImageIcon("images/bloc_poussable.png");
	ImageIcon img_oiseau = new ImageIcon("images/oiseau.png");
	ImageIcon img_snoopy = new ImageIcon("images/snoopy.png");
	ImageIcon bloc_piege = new ImageIcon("images/piege.png");
	ImageIcon img_vie = new ImageIcon("images/vie.png");
	JLabel[][] plateau = new JLabel[10][20];
	Counter compteur;
	static JButton quelBoutonEstAppuye=new JButton();
	Balle balle, balle2;
	
	int nb_niveau, nb_vie=3;
	
	int nb_score=0;
	
	//boolean fenetre_ouverte2=true;

	public Fenetre_Jeu(int nb)
	{
		/**Declaration de la nouvelle fenetre*/
		super("Snoopy - Magic Show - Jeu");
		
		
		nb_niveau = nb;
		
		/**Instantiation des 4 panels*/
		panel_bouton = new JPanel();
		legende = new JPanel();
		west = new JPanel();
		east = new JLayeredPane();
		board = new JPanel();
		north = new JPanel();
		south = new JPanel();
		
		/**Instantiation de Balle */
		balle = new Balle(15,20);
		balle2 = new Balle(300, 400);
		
		/**Panel Nord, qui contient la vie restante, le timer et le bouton pause*/
		north.setLayout(new GridLayout(1,0));
		north.setBorder(new TitledBorder("Menu :"));
		vie = new JLabel("Vie(s) : "+nb_vie);
		north.add(vie);

		temps = new JLabel(" - Temps restant : 12:00");
		north.add(temps);
		
		playerScore=new JLabel("Votre score actuel : "+nb_score);
		north.add(playerScore);
		
		playerScore.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		
		reset_jeu = new JButton("Recommencer");
		north.add(reset_jeu);
		
		textJeuPause=new JLabel();
		north.add(textJeuPause);

			
		/**TEST COMPTEUR*/
		
		compteur=new Counter(temps,60);
		compteur.start();

		
		/**Panel Ouest, qui contient les 4 boutons de direction*/
		
		/**bouton pour casser les blocs test*/
		line0 = new JPanel();
		line1 = new JPanel();
		line2 = new JPanel();
		line3 = new JPanel();
		line1.setLayout(new BoxLayout(line1, BoxLayout.LINE_AXIS));

		haut = new JButton("Haut");
		line1.add(haut);
		
		line0.setLayout(new BoxLayout(line0, BoxLayout.LINE_AXIS));
		p = new JButton("Pousser");
		line0.add(p);
		c = new JButton("Casser");
		line0.add(c);
		
		line2.setLayout(new BoxLayout(line2, BoxLayout.LINE_AXIS));
		gauche = new JButton("Gauche");
		droite = new JButton("Droite");
		line2.add(gauche);
		line2.add(droite);

		line3.setLayout(new BoxLayout(line3, BoxLayout.LINE_AXIS));
		bas = new JButton("Bas");
		line3.add(bas);
		
		/**Panel de legende, qui contient les images des blocs*/
		legende.setLayout(new BoxLayout(legende, BoxLayout.PAGE_AXIS));
		legende.setBorder(new TitledBorder("Legende :"));
		leg1 = new JLabel();
		leg1.setIcon(img_snoopy);
		leg2 = new JLabel();
		leg2.setIcon(bloc_cassable);
		leg3 = new JLabel();
		leg3.setIcon(bloc_piege);
		leg4 = new JLabel();
		leg4.setIcon(bloc_poussable);
		leg5 = new JLabel();
		leg5.setIcon(img_oiseau);
		leg6 = new JLabel();
		leg6.setIcon(img_vie);
		
		legende.add(new JLabel("Snoopy : "));
		legende.add(leg1);
		legende.add(new JLabel("Blocs cassable : "));
		legende.add(leg2);
		legende.add(new JLabel("Bloc piege : "));
		legende.add(leg3);
		legende.add(new JLabel("Bloc poussable : "));
		legende.add(leg4);
		legende.add(new JLabel("Oiseau : "));
		legende.add(leg5);
		legende.add(new JLabel("Vie : "));
		legende.add(leg6);
				
		/**Panel qui contient les 4 boutons, en position croix*/
		panel_bouton.setLayout(new BoxLayout(panel_bouton, BoxLayout.PAGE_AXIS));
		
		panel_bouton.add(line0);
		panel_bouton.add(line1);
		panel_bouton.add(line2);
		panel_bouton.add(line3);
		
		/**Ajout des boutons au panel Sud Ouest*/
		west.setLayout(new BorderLayout());
		west.add(panel_bouton, BorderLayout.SOUTH);
		west.add(legende, BorderLayout.CENTER);
		
			
		/**Panel Est, qui contient le tableau de JLabel qui contiennent les images*/
		board.setLayout(new GridLayout(10,20));
		board.setBackground(Color.white);
		board.setOpaque(true);
		
		east.add(board, new Integer(0));
		east.add(balle.panel, new Integer(1));
		east.add(balle2.panel, new Integer(2));
		east.setLayout(new FlowLayout());
		east.setVisible(true);
				
		/**Initialisation des JLabel*/
		for(int i=0 ; i<10; i++)
		{
			for(int j =0 ; j<20; j++)
			{
				plateau[i][j] = new JLabel();
			}
		}
		
		/**Panel Sud, qui contient le bouton Quitter*/
		quit = new JButton("Fermer");
		south.add(quit);
		
		/**Ajout des panels au Frame principal*/
		this.add(west, BorderLayout.WEST);
		this.add(north, BorderLayout.NORTH);
		this.add(south, BorderLayout.SOUTH);
		this.add(east, BorderLayout.CENTER);
		balle.initBalle();
		
		this.pack();
		this.setSize(1200, 630);
		this.setLocation(100, 100);
		this.setVisible(true);
	}
	
	protected void setNiveau(int niveau)
	{
		nb_niveau = niveau;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		//int test=compteur.stopCount();
		//System.out.println("le compteur s'est arret� avec la valeur : "+test);
		this.dispose();
		
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		//int test=compteur.stopCount();
		//System.out.println("le compteur s'est arret� avec la valeur : "+test);
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	protected void setNb_vie(int a) {
		this.nb_vie=a;
		
	}
	
	public int displayScore() {
		int score=compteur.score();
		System.out.println("valeur: "+score);
		this.nb_score+=score;
		System.out.println("valeur: "+nb_score);
		return this.nb_score;
	}
	
}

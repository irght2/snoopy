package snoopy;

public class Affichage extends Main {
	
	Menu menu;
	Fenetre_Jeu fenetre_jeu;
	
	public Affichage() {
		
	}
	
	protected void newMenu()
	{
		menu = new Menu();
	}
	
	protected void newFenetre_Jeu(int niveau)
	{
		fenetre_jeu = new Fenetre_Jeu(niveau);
	}
	
	protected void ajoutActionListenerMenu()
	{
		/**Ajout des ActionListener pour chaque bouton du menu*/		

		menu.charger.addActionListener(this);
		menu.jouer.addActionListener(this);
		menu.mdp.addActionListener(this);
		menu.regles.addActionListener(this);
		menu.scores.addActionListener(this);
		menu.quitter.addActionListener(this);
		menu.addKeyListener(this);
	}
	
	@SuppressWarnings("static-access")
	protected void ajoutActionListenerFenetre_Jeu()
	{
		/**Ajout des ActionListener pour chaque bouton de la fenetre de jeu*/
		fenetre_jeu.reset_jeu.addActionListener(this);
		fenetre_jeu.haut.addActionListener(this);
		fenetre_jeu.gauche.addActionListener(this);
		fenetre_jeu.droite.addActionListener(this);
		fenetre_jeu.bas.addActionListener(this);
		fenetre_jeu.quit.addActionListener(this);
		fenetre_jeu.c.addActionListener(this);
		fenetre_jeu.p.addActionListener(this);
		fenetre_jeu.addKeyListener(this);
		fenetre_jeu.setFocusable(true);

		//fenetre_jeu.setAutoRequestFocus(true);
	}
}

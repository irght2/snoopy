package snoopy;

/** classe Pion*/

public class Pion{
	
	/** Declaration attributs*/
	int x, y;
	String nom;
	
	/**Constructeur par defaut*/
	public Pion()
	{
		x = 0;
		y = 0;
		nom = null;
	}
	
	/** Constructeur surcharge
	 * @param x1
	 * New value*/
	public Pion(int x1, int y1, String nom1)
	{
		x = x1;
		y = y1;
		nom = nom1;
	}
	
	public Pion(String nom1) {
		x=0;
		y=0;
		nom=nom1;
	}
	
	/**Getters
	 * @return x
	 * value of position*/
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public String getNom()
	{
		return nom;
	}
	
	/**Setters
	 * @param x1
	 * new value of x*/
	public void setX(int x1)
	{
		x = x1;
	}
	
	public void setY(int y1)
	{
		y = y1;
	}
	public void setNom(String nom1)
	{
		nom = nom1;
	}

}

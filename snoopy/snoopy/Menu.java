package snoopy;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;


import javax.swing.*;
import javax.swing.border.TitledBorder;

/**Classe Menu*/
@SuppressWarnings("serial")
public class Menu extends JFrame{
	
	/**Attributs*/
	JLabel title, subtitle, sub, image, texte;
	JButton jouer, charger, mdp, scores, regles, quitter, submit;
	JTextField mdp_area;
	JFrame mdp_frame;
	
	
	
	public Menu()
	{
		/**On donne un titre a la fenetre*/
		super("Snoopy - Magic Show - Menu");
		/**Ajout du WindowListener a la fenetre*/
		
		/**Ajout titre du jeu*/
		title = new JLabel("SNOOPY");
		sub = new JLabel("MAGIC SHOW");
		
		/**On definie la police de caractere pour le titre*/
		Font font = new Font("Rockwell", Font.BOLD, 20);
		
		/**On modifie les proprietes du titre*/
		title.setFont(font);
		title.setForeground(Color.RED);
		title.setAlignmentX(CENTER_ALIGNMENT);
		sub.setFont(font);
		sub.setForeground(Color.BLACK);
		sub.setAlignmentX(CENTER_ALIGNMENT);
		
		/**On met les deux morceaux du titre l'un sur l'autre*/
		Box titre = Box.createVerticalBox();
		titre.add(title);
		titre.add(sub);
		add(titre, BorderLayout.NORTH);
		
		/**On instancie les differents boutons*/
		jouer = new JButton("Jouer");
		charger = new JButton("Charger une partie");
		mdp = new JButton("Mot de passe");
		scores = new JButton("Scores");
		regles = new JButton("Regles du jeu");
		
		/**On declare un panel pour regrouper les boutons sur la droite de la fenetre*/
		JPanel menu = new JPanel();
		menu.setBorder(new TitledBorder("Menu:"));
		menu.setLayout(new GridLayout(0,1));
		menu.add(jouer);
		menu.add(charger);
		menu.add(mdp);
		menu.add(scores);
		menu.add(regles);
		this.add(menu, BorderLayout.CENTER);
				
		/**On charge l'image pour l'affichage de l'image a gauche du menu*/
		ImageIcon icone = new ImageIcon("images/snoopy_menu.jpeg");
		image = new JLabel(icone);		
		add(image, BorderLayout.WEST);
		
		/**Ajout du bouton quitter en bas de fenetre*/
		quitter = new JButton("Quitter");
		add(quitter, BorderLayout.SOUTH);
		
		this.pack();		
		this.setVisible(true);
		
	}

	
	
	

}

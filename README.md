# Snoopy !

But : reccuperer les 4 oiseaux du niveau sans se faire toucher par le/les balle(s) et avant les 60 secondes

- Menu :
	1. Jouer
	2. Charger une partie
	3. Mot de passe
	4. Scores
	5. Quitter
	
- Snoopy peut :
	- pousser des blocs
	- casser des blocs
	
- Types de blocs 
	- blocs poussables
	- blocs cassables
	- blocs P : invincibilité
	- blocs qui disparaissent et apparaissent
	- balles
	
- Symboles :
	- Snoopy : S
	- balle : B
	- oiseau : O
	- bloc poussable : P
	- bloc cassable : C
	- bloc piégé : T
	
- Touches :
	- casser : touche a
	- sauvegarder : touche s
	- pause : touche p

- Comportement :
	- Snoopy ne peut se déplacer qu'en ligne ou en colonne
	- la/les balle(s) se déplace(ent) en diagonale
	- blocs piégés tuent immédiatement
	- 0 vies : Game Over et retour au menu
	
- Divers : 
	- score niveau = temps restant * 100
	- score total = somme des scores niveau
	- 3 vies par défaut
	- niveau à résoudre en moins de 60 secondes sinon -1 vie
	- dimension du tableau : 20*10
	
- Classes :
	- Immobile
		- Items (invincibilité, vie, oiseau)
		- Bloc poussable
		- Bloc cassable
		
	- Mobile
		- Snoopy
		- Balle
		- Ennemi
	